// console.log("Hello World!");

// [SECTION] - Objects
/* SYNTAX: let/const objectName = {
	keyA: valueA,
	keyB: valueB
}*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/literal notation:");
console.log(cellphone);
console.log(typeof cellphone);

// This is an object

function Laptop(name, manufactureDate){
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constructors: ");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating object using object constructors: ");
console.log(myLaptop);

// We cannot create new object without the instance or the "new" keyword
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating object using object constructors: ");
console.log(oldLaptop); //return undefined value.

// Creating empty objects
let computer = {}
console.log(computer);

let array = [laptop, myLaptop];

// This is confusing because of the array method
console.log(array[0]["name"]);
// The proper way of accessing object property value is by using dot notation
console.log(array[0].name);

// [SECTION] - Initializing, Deleting, Re-assigning Object Properties

let car = {};

// Adding new key and value
car.name = "Honda Civic";
console.log(car);

// We can create objects with:

// {} - Object Literal - Great for creating dynamic objects
let pokemon1 = {
	name: "Pikachu",
	type: "Electric"
}
let pokemon10 = {
	name: "Mew",
	type: ["Electric","Normal"]
}
let pokemon25 = {
	name: "Charizard",
	level: 30
}

// Contructor function - allows us to create objects with a defined structure

function Pokemon(name,type,level){

	//this - keyword used to refer to the object to be created with the constructor function
	this.name = name,
	this.type = type,
	this.level = level
}

// new keyword allows us to create an instance of an object  with a constructor function.

let pokemonInstance1 = new Pokemon("Bulbasaur", "Grass", 32);

// Objects can have arrays as properties:

let professor1 = {
	name: "Romenick Garcia",
	age: 25, 
	subjects: ["MongoDB", "HTML", "CSS", "JS"]
}

// We can access object properties with dot notation
console.log(professor1.subjects);

// What if we want to add an item in the array within the object?
// What if we want to add an "NodeJs" as subject for  professor1?
professor1.subjects.push("NodeJS");

// Mini-Activity: Display in the console each subject from professor1's subjects array:

// Approach 1 - access the items from the array using its index:
console.log(professor1.subjects[0]);
console.log(professor1.subjects[1]);
console.log(professor1.subjects[2]);
console.log(professor1.subjects[3]);
console.log(professor1.subjects[4]);

// Approach 2 - we can also us forEach() to display each item in the array:
professor1.subjects.forEach(subject => console.log(subject));
professor1.subjects.forEach(function (subject){
	console.log(subject);
});

let dog1 = {
	name: "Bantay",
	breed: "Golden Retriever"
}
let dog2 = {
	name: "Bolt",
	breed: "Aspin"
};

// Array of objects:

let dogs = [dog1,dog2];

// How can we display the details of the first item in the dogs array?
// You can use the index number of the item to access it from the array:
console.log(dogs[0]);
// How can we display the value of the name of the property of the second item?
console.log(dogs[1].name);
console.log(dogs[1]["name"]);

// What if we want to delete the second item from the dogs array?
dogs.pop();

dogs.push({

	name: "Whitey",
	breed: "Shih Tzu"

	});

// Initialize, delete, update object properties

let supercar1 = {};

console.log(supercar1);

// Initialize propertoes and values with our empty object:
supercar1.brand = "Porche";

console.log(supercar1);

supercar1.model  = "Porche 911";
supercar1.price = 182900;

console.log(supercar1);
console.log(supercar1.price);

// Delete object properties with the delete keyword
delete supercar1.price;

console.log(supercar1);

// Update the valies of an object using the dot notation:
supercar1.model = "718 Boxster";

console.log(supercar1);

pokemonInstance1.type = "Grass,Normal";

// Object Methods
// Functions in an object
// This is useful for creating functions that are associated to a specific object

let person1 = {
	name: "Joji",
	talk: function(){
		console.log("Hello!");
	}
};

person1.talk();
// talk();

let person2 = {
	name: "Joe",
	talk: function(){
		console.log("Hello, World!");
	}
};

person2.talk();

person2.walk = function(){
	console.log("Joe has walked 500 miles just to be the man that walked 1000 miles to be at you door.")
}

// walk();
// person1.walk();
person2.walk();

// this keyword in an object method refers to the current object where the method is.

person1.introduction = function(){{
	console.log("Hi! I am " + " " + this.name + "!");
}}

person1.introduction();


person2.introduction = function(){
	console.log(this);
}

person2.introduction();

// Mini-Activity
// Create a new object as student1
// The object should have the following properties:
// name, age, address
// Add 3 methods for student1
// introduceName = which will display the student1's name as:
// "Hello! My name is <name of student1>"
// introduceAge = which will display the age of student1 as:
// "Hello! I am <age fo student1> years old."
// // introduceAddress = which will display the address of student1 as:
// "Hello! I am <nameofstudent1>. I live in <addressofstudent1> years old."
// Invoke all 3 methods and send your output in the hangouts.


let student1 = {
	name: "Rojinald James Turingan",
	age: 39,
	address: "Binan, Laguna",
	introduceName: function(){
		console.log("Hello! My name is " + this.name + "!");
	},
	introduceAge : function(){
		console.log("Hello! I am " + this.age + " years old.");
	},
	introduceAddress: function(){
			console.log("Hello! I am " + this.name + "." + " I live in " + this.address + ".");
	}
};

student1.introduceName();
student1.introduceAge();
student1.introduceAddress();

function Student(name,age,address){
	this.name = name;
	this.age = age;
	this.address = address;

	// We can also add methods to our constructor function
	this.introduceName = function(){
		console.log("Hello! My name is " + this.name + "!");
	};
	introduceAge = function(){
		console.log("Hello! I am " + this.age + " years old.");
	};
	introduceAddress = function(){
			console.log("Hello! I am " + this.name + "." + " I live in " + this.address + ".");
	}
	// We can also add methods that take other objects as an argument
	this.greet = function(person){
		// person properties are accessible in the method
		console.log("Good Day, " + person.name + "!");
	}

};

let newStudent1 = new Student("Rojinald",39,"Laguna");
// console.log(newStudent1);

let newStudent2 = new Student("Jeremiah",26,"Lucena, Quezon");

newStudent1.greet(newStudent2);
newStudent2.greet(newStudent1);

// Mini-Activity
// Create a new constructor function called Dog which is able  to create objects with the following properties:
// name, breed
// The constructor function should also have 2 methods:
// call() - is a method which will allow us to display the following message:
// "Bark Bark Bark"
// greet() - is a method which allows us to greet another person. This method should be able to receive an object which has a name property. Upon invoking the greet() method, it should display:
// "Bark, bar, <nameOfPerson>"

// Create an instance/object from the Dog constructor
// Invoke its call() method
// Invoke its greet method to greet newStudent2.

// Take a screenshot of your output and send it to the hangouts.

function dog(name,breed){
	this.name = name;
	this.breed= breed;
	

	this.call = function(){
		console.log("Bark Bark Bark");
	}
	this.greet = function(person){
		// We are assuming that the data passed as person is an object with a name property
		// hasOwnProperty() method will check if  the object passed has the indicated property

		console.log(person.hasOwnProperty("name"));
			// console.log("Bark, bark, " + person.name + "!");
	}

}
let newDog = new dog("Buddy","German Shepherd");

newDog.call();

newDog.greet(newStudent2);
newDog.greet(supercar1);

// When creating a function that is supposed to receive data/argument
// It is a best practice to log the parameter first.
function sample1(sampleParameter){
	// this argument you passed during invocation is now represented by the parameter
	console.log(sampleParameter.name);
	newDog.name = "Gary";
}

// argument is the data passed during function invocation
sample1(newDog);
console.log(newDog);
// sample1(25000);